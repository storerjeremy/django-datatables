class FieldError(Exception):
    """Some kind of problem with a datatable field."""
    pass

class ValidationError(Exception):
    """A validation issue with a datatable field."""
    pass
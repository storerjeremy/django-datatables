import csv
from django_datatables.writers.base import BaseWriter
from django_datatables.writers.helpers.unicode import UnicodeWriter

class UnicodeCsvWriter(BaseWriter):
    
    def write(self, f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL):
        """
        Write the csv to path
        
        @param f: Either a file like object or a file path
        @return UnicodeWriter: The UnicodeWriter instance used to write the csv.
        """
        if not hasattr(f, 'read'):
            handle = open(f, 'wb')
        else:
            handle = f
        
        writer = UnicodeWriter(handle, delimiter=delimiter, quotechar=quotechar, quoting=quoting)
        writer.writerow(self.datatable.titles())
        try:
            for item in self.qs:
                row = []
                for field in self.datatable.fields():
                    row.append(u"%s" % field.widget.render(self, field.name, field.traverse_for_value(item), item))
                writer.writerow(row)
        except UnicodeDecodeError as e:
            e.reason += ' - for row: %s' % ', '.join(row)
            raise e
        finally:
            if handle != f:
                # If we opened the file, close it.
                handle.close()
from django.conf.urls.defaults import *
from django_datatables.views import DatatableView

# Example view definition
# urlpatterns = patterns('',
#     url(r'^view/(?P<app>[\w|.]+)/(?P<datatable_name>\w+)/$', 
#         view=DatatableView.as_view(
#             datatable=MyDataTable()
#         ), 
#         name='my_name'
#     ),
# )

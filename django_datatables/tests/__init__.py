import django_datatables
from datetime import date
from django.contrib.contenttypes.models import ContentType
from django_datatables.widgets import ActionWidgetImageButton

class ContentTypeDataTable(django_datatables.Datatable):
    name = django_datatables.CharField(sorting=True, filter=True, data_tip="Here is a tip")
    app_label = django_datatables.CharField(sorting=True, filter=True, data_tip="I can reference other columns: name: {{ object.name }}")
    model = django_datatables.CharField(sorting=True, filter=True)
    today = django_datatables.DateField(filter=True)
    get_actions = django_datatables.ActionsField(title='Actions')
    
    class Meta:
        verbose_name = 'Content Type'
        description = 'A listing of content types'
    
    def queryset(self):
        """
        Returns an iterator of the data.
        """
        return self.Item.objects.all()

    class Item(ContentType):
        """
        The queryset returns one of these objects for each row in the datatable.
        """
        
        class Meta:
            proxy = True
            # Very important that the following is unique across your app
            app_label = 'datatables_tests_contenttype'
        
        def today(self):
            return date.today()
        
        def get_row_class(self):
            if self.name == 'ContentType':
                return 'info'
        
        def get_actions(self):
            """
            Return a list of context aware actions.
            """
            actions = []
            
            if self.name == 'group':
                actions.append(ActionWidgetImageButton(title='Danger!', btn_class="btn-danger", icon_class="icon-bullhorn icon-white", href="#"))
            elif self.name == 'user':
                actions.append(ActionWidgetImageButton(title='W00T', btn_class="btn-info", icon_class="icon-question-sign", href="#"))
    
            return actions
import sys, re
import logging
from django.utils import six
from django.utils.functional import curry
from django.conf import settings
from django.utils.translation import string_concat
from django.utils.text import slugify
import copy

get_verbose_name = lambda name: re.sub('(((?<=[a-z])[A-Z])|([A-Z](?![A-Z]|$)))', ' \\1', name).lower().strip()

DEFAULT_NAMES = ('verbose_name', 'app_label', 'slug', 'description')

class Options():
    def __init__(self, meta, app_label=None):
        self.app_label = app_label
        self.meta = meta
        self.fields = []
        self.slug = None
        self.verbose_name, self.verbose_name_plural = None, None
        self.description = None

    def add_field(self, field):
        self.fields.append(field)

    def contribute_to_class(self, cls, name):
        cls._meta = self
        self.datatable = cls
        self.app = re.sub('\.datatables\.\w+$', '', cls.__module__)
        self.installed = self.app in settings.INSTALLED_APPS
        # First, construct the default values for these options.
        self.object_name = cls.__name__
        self.module_name = self.object_name.lower()
        self.datatable_name = cls.__module__.split('.')[-1]
        self.verbose_name = u'%s - %s' % (get_verbose_name(self.app_label).title(), get_verbose_name(cls.__module__.split('.')[-1]).title())
        self.slug = slugify(self.verbose_name)

        # Next, apply any overridden values from 'class Meta'.
        if self.meta:
            meta_attrs = self.meta.__dict__.copy()
            for name in self.meta.__dict__:
                # Ignore any private attributes that Django doesn't care about.
                # NOTE: We can't modify a dictionary's contents while looping
                # over it, so we loop over the *original* dictionary instead.
                if name.startswith('_'):
                    del meta_attrs[name]
            for attr_name in DEFAULT_NAMES:
                if attr_name in meta_attrs:
                    setattr(self, attr_name, meta_attrs.pop(attr_name))
                elif hasattr(self.meta, attr_name):
                    setattr(self, attr_name, getattr(self.meta, attr_name))

            # Any leftover attributes must be invalid.
            if meta_attrs != {}:
                raise TypeError("'class Meta' got invalid attribute(s): %s" % ','.join(meta_attrs.keys()))
        else:
            self.verbose_name_plural = string_concat(self.verbose_name, 's')
        del self.meta

    def _prepare(self, datatable):
        pass

class DatatableBase(type):
    """
    Base datatable - all datatables should extend this.
    
    Borrowed heavily from django's ModelBase
    """
    def __new__(cls, name, bases, attrs):
        super_new = super(DatatableBase, cls).__new__
        # six.with_metaclass() inserts an extra class called 'NewBase' in the
        # inheritance tree: Model -> NewBase -> object. Ignore this class.
        parents = [b for b in bases if isinstance(b, DatatableBase) and
                not (b.__name__ == 'NewBase' and b.__mro__ == (b, object))]
        if not parents:
            # If this isn't a subclass of Model, don't do anything special.
            return super_new(cls, name, bases, attrs)

        # Create the class.
        module = attrs.pop('__module__')
        new_class = super_new(cls, name, bases, {'__module__': module})
        attr_meta = attrs.pop('Meta', None)
        if not attr_meta:
            meta = getattr(new_class, 'Meta', None)
        else:
            meta = attr_meta
        base_meta = getattr(new_class, '_meta', None)

        # Ensure we reset the creation counter for each datatable
        from django_datatables.fields import Field
        Field.creation_counter = 0

        if getattr(meta, 'app_label', None) is None:
            # Figure out the app_label by looking one level up.
            # For 'django.contrib.sites.models', this would be 'sites'.
            datatable_module = sys.modules[new_class.__module__]
            kwargs = {"app_label": datatable_module.__name__}
        else:
            kwargs = {}

        new_class.add_to_class('_meta', Options(meta, **kwargs))

        field_names = set([f.name for f in new_class._meta.fields])

        # Add all attributes to the class.
        for obj_name, obj in attrs.items():
            new_class.add_to_class(obj_name, obj)
            #setattr(new_class, obj_name, obj)

        for base in parents:
            if not hasattr(base, '_meta'):
                # Things without _meta aren't functional datatables, so they're
                # uninteresting parents.
                continue

            parent_fields = base._meta.fields
            # Check for clashes between locally declared fields and those
            # on the base classes (we cannot handle shadowed fields at the
            # moment).
            for field in parent_fields:
                if field.name in field_names:
                    raise Exception('Local field %r in class %r clashes '
                                     'with field of similar name from '
                                     'base class %r' %
                                        (field.name, name, base.__name__))

            for field in parent_fields:
                obj = copy.deepcopy(field)
                new_class.add_to_class(field.name, obj)
                setattr(new_class, field.name, obj)

            # Pass any non-abstract parent classes onto child.
            # new_class._meta.parents.update(base._meta.parents)

        # Ensure fields are in the correct order...
        new_class._meta.fields.sort()
        new_class._prepare()
        
        return new_class
    
    def add_to_class(cls, name, value):
        if hasattr(value, 'contribute_to_class'):
            value.contribute_to_class(cls, name)
        else:
            setattr(cls, name, value)
    
    def _prepare(cls):
        """
        Creates some methods/attrs once self._meta has been populated.
        """
        opts = cls._meta
        opts._prepare(cls)

        # Give the class a docstring -- its definition.
        if cls.__doc__ is None:
            cls.__doc__ = "%s(%s)" % (cls.__name__, ", ".join([f.attname for f in opts.fields]))

        if hasattr(cls, 'get_absolute_url'):
            cls.get_absolute_url = update_wrapper(curry(get_absolute_url, opts, cls.get_absolute_url),
                                                  cls.get_absolute_url)

class Datatable(six.with_metaclass(DatatableBase, object)):

    def __init__(self):
        for field in self.fields():
            # Ensure the datatable on a field points to self
            field.datatable = self

    def queryset(self):
        """
        Return the queryset for this datatable.
        """
        raise NotImplementedError

    def fields(self, attr=None, value=None):
        """
        Return a list of fields optionally filtering the list using attr=value
        
        @param attr: The attribute to filter
        @param value: The value of the attribute
        @return: List of fields
        """
        fields = self._meta.fields
        if attr:
            return [field for field in fields if getattr(field, attr) == value]
        return fields 
    
    def field(self, name):
        """
        Retrieve a field by its name.
        """
        fields = [field for field in self.fields() if field.name == name]
        if fields:
            return fields[0]
    
    def titles(self):
        """
        Return a list of field titles.
        """
        return [field.title for field in self._meta.fields]
    
    def get_qs_for_term(self, term):
        """
        Get a queryset object that uses term to filter the entire datatable.
        """
        if not term or len(term) == 0:
            return None
        
        qs_params = None
        for field in self.fields('filter', True):
            q = field.get_qs_for_term(term)
            if q:
                qs_params = qs_params | q if qs_params else q
        return qs_params

    def set_request(self, request):
        """
        Set the django HTTP request.
        """
        self.request = request

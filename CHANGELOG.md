# Release 0.0.5

- Optimisations - datatable instance isn't created until the 'initialize' method called by the view.

# Release 0.0.4

- Request is now available from the datatable
- Added FontAwesome icon/button rendering classes

# Release 0.0.3

- Nada... 

# Release 0.0.2

- Added git-tools

# Release 0.0.1

- Intial release.

